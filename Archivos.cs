﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CtrlAdminArchivos
{
    public class ControladorArchivos
    {

        #region Atributos

        static List<FileInfo> listaArchivos = new List<FileInfo>();

        #endregion
        #region Constructores
        #endregion
        #region Propiedades
        #endregion
        #region Metodos

        /// <summary>
        /// Método que permite eliminar los archivos que se indican en la lista
        /// </summary>
        /// <param name="listaNombresArchivos">Lista que contiene la información de los archivos que se desean eliminar</param>
        public static void BorrarArchivos(List<string> listaNombresArchivos)
        {
            foreach (string rutaArchTemp in listaNombresArchivos)
            {
                File.Delete(rutaArchTemp);
            }
        }

        /// <summary>
        /// Método que permite generar un nombre de archivo aleatorio
        /// </summary>
        /// <param name="numArchivo">Número de archivo</param>
        /// <param name="extension">Extensión que tendrá el nombre de archivo</param>
        /// <param name="rutaArchivo">Ruta que contiene donde se creará el archivo</param>
        /// <returns>Cadena que contiene el nombre y ruta del archivo</returns>
        public static string GenerarNombreAleatorio(int numArchivo, string extension, string rutaArchivo)
        {
            string nombreArchivo = Path.GetRandomFileName();
            nombreArchivo = Path.ChangeExtension(numArchivo + "_" + nombreArchivo, extension);
            nombreArchivo = Path.Combine(rutaArchivo, nombreArchivo);

            return nombreArchivo;
        }

        public static string GenerarNombreAleatorio(string extension, string rutaArchivo)
        {
            string nombreArchivo = Path.GetRandomFileName();
            nombreArchivo = Path.ChangeExtension(nombreArchivo, extension);
            nombreArchivo = Path.Combine(rutaArchivo, nombreArchivo);

            return nombreArchivo;
        }

        /// <summary>
        /// Método que permite limpiar una cadena de texto eliminando los caracteres no validos
        /// en un nombre de archivo
        /// </summary>
        /// <param name="nombreArchivo">Nombre de archivo a limpiar</param>
        /// <returns>Cadena que contiene el nombre del archivo depurado</returns>
        public static string LimpiarNombreArchivoCaracteresNoValidos(string nombreArchivo)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));

            return r.Replace(nombreArchivo, "");
        }

        /// <summary>
        /// Método que permite obtener todos los archivos que no tienen extensión
        /// en base a un patron de busqueda
        /// </summary>
        /// <param name="rutaBusqueda">Objeto que contiene la información de la ruta donde se realizará la búsqueda</param>
        /// <param name="patronBusqueda">Patrón de búsqueda para localizar los archivos</param>
        private static void ObtenerArchivosSinExtension(DirectoryInfo rutaBusqueda, string patronBusqueda)
        {
            try
            {
                // recorre los archivos que se encuentran dentro de la ruta y que no tienen extensión
                foreach (FileInfo arch in rutaBusqueda.GetFiles(patronBusqueda).Where(arch => string.IsNullOrEmpty(arch.Extension)))
                {
                    // agrega a la lista el archivo localizado
                    listaArchivos.Add(arch);
                }
            }
            catch
            {
                // se detecta cuando un archivo no se puede leer y se evita el mensaje de error
            }

            try
            {

                // recorre las carpetas que se encuentra dentro de la ruta de búsqueda
                foreach (DirectoryInfo folder in rutaBusqueda.GetDirectories())
                {                
                    // invoca la búsqueda de los archivos sin extensión de la carpeta
                    ObtenerArchivosSinExtension(folder, patronBusqueda);
                }
            }
            catch
            {
                // se detecta cuando una carpeta no se puede leer y se evita el mensaje de error
            }
        }
        /// <summary>
        /// Método que permite obtener los archivos sin extensión de una determinada unidad de almacenamiento
        /// </summary>
        /// <param name="unidad">Cadena de texto que contiene el nombre de la unidad de almacenamiento</param>
        /// <returns>Lista de objetods que contiene la información de los archivos localizados</returns>
        public static List<FileInfo> ObtenerArchivosSinExtension(string unidad)
        {
            // crea la lista que almacenará la información
            listaArchivos = new List<FileInfo>();
            // crea un nuevo objeto en base a la unidad donde se desea realizar la búsqueda
            DirectoryInfo di = new DirectoryInfo(unidad);

            // invoca la funcionalidad para obtener los archivos sin extensión
            ObtenerArchivosSinExtension(di, "*");

            // retorna la lista de archivos
            return listaArchivos;
        }
        /// <summary>
        /// Método que permite obtener la lista de las unidades de almacenamiento del equipo
        /// </summary>
        /// <returns>Lista de texto que contiene la información de las unidades localizadas</returns>
        public static List<string> ObtenerListaUnidadesEquipo()
        {
            // obtiene la lista de las unidades de almacenamiento del equipo
            DriveInfo[] listaUnidades = DriveInfo.GetDrives();

            // devuelve una lista de los nombres de las unidades localizadas
            return listaUnidades.Select(d => d.Name).ToList();
        }

        #endregion
        #region Delegados
        #endregion  
                                
    }
}
