﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CtrlAdminArchivos
{
    public class Carpetas
    {
        #region Atributos

        private string _rutaCarpeta;

        #endregion

        #region Propiedades

        /// <summary>
        /// Propiedad que contiene la lista de las carpetas
        /// internas a partir de la carpeta que se está procesando
        /// </summary>
        public List<string> ListaCarpetasInternas
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor base
        /// </summary>
        public Carpetas()
        {
            ListaCarpetasInternas = new List<string>();
        }

        /// <summary>
        /// Constructor con ruta de carpeta a procesar
        /// </summary>
        /// <param name="rutaCarpeta">Ruta de la carpeta que se va a procesar</param>
        public Carpetas(string rutaCarpeta)
        {
            _rutaCarpeta = rutaCarpeta;
            ListaCarpetasInternas = new List<string>();

            // invoca funcionalidad para obtener los nombres de las carpetas internas
            ActualizarListaCarpetasInternas();
        }

        #endregion

        #region Metodos

        /// <summary>
        /// Método que obtiene los nombres de las carpetas
        /// internas de la carpeta que se está procesando
        /// </summary>
        private void ActualizarListaCarpetasInternas()
        {
            // verifica si la ruta de la carpeta es válida
            if (!EsRutaCarpetaValida())
                return;

            // obtiene la lista de los nombres de las carpetas internas
            string[] carpetas = Directory.GetDirectories(_rutaCarpeta);

            // recorre la lista de las carpetas y las agrega a la propiedad de la lista de Carpetas Internas
            foreach (string rutaCarpetaHija in carpetas)
                ListaCarpetasInternas.Add(rutaCarpetaHija);
        }

        /// <summary>
        /// Método que permite crear una nueva carpeta a partir de una determinada ruta
        /// </summary>
        /// <param name="rutaCarpeta"></param>
        public void Crear(string rutaCarpeta)
        {
            // crea la nueva carpeta en el disco duro
            System.IO.Directory.CreateDirectory(rutaCarpeta);
        }

        /// <summary>
        /// Método que permite borrar una carpeta en el disco duro
        /// </summary>
        /// <param name="rutaCarpeta">Ruta de la carpeta que se desea eliminar</param>
        /// <returns>Mensaje de error en caso de que falle la eliminación</returns>
        public string Borrar(string rutaCarpeta)
        {
            try
            {
                // invoca la funcionalidad para borrar la carpeta
                Directory.Delete(rutaCarpeta, true);

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Método que permite verificar si la ruta
        /// de la carpeta es valida
        /// </summary>
        /// <returns>Resultado de la validación</returns>
        public bool EsRutaCarpetaValida()
        {
            // valida si existe una ruta a validar
            if (string.IsNullOrEmpty(_rutaCarpeta))
                return false;

            // verifica si la carpeta existe en el disco duro
            return Directory.Exists(_rutaCarpeta);
        }

        /// <summary>
        /// Método que permite verificar si la ruta que se envía como parámetro
        /// es valida
        /// </summary>
        /// <param name="rutaCarpeta">Ruta de la carpeta a validar</param>
        /// <returns>Resultado de la validación</returns>
        public bool EsRutaCarpetaValida(string rutaCarpeta)
        {
            // actualiza la ruta de la carpeta a verificar
            _rutaCarpeta = rutaCarpeta;

            // invoca funcionalidad para validar la carpeta
            return EsRutaCarpetaValida();
        }

        /// <summary>
        /// Método que permite mover la información de una carpeta a otra
        /// </summary>
        /// <param name="rutaCarpOrigen">Ruta de la carpeta origen</param>
        /// <param name="rutaCarpDestino">Ruta de la carpeta destino</param>
        /// <returns>Mensaje de error en caso de que suceda algún problema</returns>
        public string MoverInformacion(string rutaCarpOrigen, string rutaCarpDestino)
        {
            try
            {
                // valida si existe la carpeta destino, sino la crea
                if (!Directory.Exists(rutaCarpDestino))
                    Crear(rutaCarpDestino);                

                // obtiene la lista de archivos y los copia a la nueva carpeta
                string[] archivos = Directory.GetFiles(rutaCarpOrigen);
                foreach (string archivo in archivos)
                {
                    string nomArch = Path.GetFileName(archivo);

                    // crea la ruta de la nueva carpeta con el nombre del archivo
                    string dest = Path.Combine(rutaCarpDestino, nomArch);

                    // invoca el copiado del archivo
                    File.Copy(archivo, dest, true);
                }

                // obtiene la lista de las carpetas internas de la carpeta origen
                string[] carpetas = Directory.GetDirectories(rutaCarpOrigen);

                // recorre la lista de las carpetas internas
                foreach (string carpeta in carpetas)
                {
                    string nomCarp = Path.GetFileName(carpeta);
                    string dest = Path.Combine(rutaCarpDestino, nomCarp);

                    // invoca recursivamente la funcionalidad de mover la información de las carpetas hijas
                    MoverInformacion(carpeta, dest);
                }

                return string.Empty;
                
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion

    }
}
